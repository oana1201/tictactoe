#include "M_TM.h"
#include <fstream>
#include <iostream>
#include <regex>
#include <random>
#include <chrono>

void M_TM::Initialization(char player)
{
	Ki = { 'C', player, '1', '2', '3', '4', '5', '6', '7', '8', '9', '$' };
	Kp = Ko = { 'C', 'b', 'b', 'b', 'b', 'b', '$' };
	Kw = { 'C',
		'1', '2', '3', 'T',
		'4', '5', '6', 'T',
		'7', '8', '9', 'T',
		'1', '4', '7', 'T',
		'2', '5', '8', 'T',
		'3', '6', '9', 'T',
		'1', '5', '9', 'T',
		'3', '5', '7', '$'
	};
	i = 1;
	k1 = k2 = w = 0;

	Board = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
}

void M_TM::ReadFromFile(const std::string& myFile)
{
	std::ifstream in(myFile);
	if (in.is_open()) {
		// Q
		std::string aux;
		getline(in, aux);
		std::regex rgx("\\s+");
		std::sregex_token_iterator iter(aux.begin(), aux.end(), rgx, -1);
		std::sregex_token_iterator end;
		for (; iter != end; ++iter)
			Q.push_back(*iter);

		//Sigma
		getline(in, Sigma);
		Sigma = std::regex_replace(Sigma, rgx, "");

		//Gama
		getline(in, Gama);
		Gama = std::regex_replace(Gama, rgx, "");

		//q0
		getline(in, q0);

		//f
		getline(in, aux);
		std::sregex_token_iterator iter2(aux.begin(), aux.end(), rgx, -1);
		for (; iter2 != end; ++iter2)
			f.push_back(*iter2);

		// Lambda
		getline(in, b);

		//Delta
		int nrTranzitii;
		in >> nrTranzitii;
		char s1, s2, s3, s4, e1, e2, e3, e4, d1, d2, d3, d4;
		std::string sStart, sEnd;
		for (int index = 0; index < nrTranzitii; ++index) {
			in >> sStart >> s1 >> s2 >> s3 >> s4 >> sEnd >> e1 >> e2 >> e3 >> e4 >> d1 >> d2 >> d3 >> d4;
			if (Delta.find(sStart + s1 + s2 + s3 + s4) != Delta.end())
				Delta.at(sStart + s1 + s2 + s3 + s4).push_back(sEnd + e1 + e2 + e3 + e4 + d1 + d2 + d3 + d4);
			else {
				std::vector<std::string> vec;
				vec.push_back(sEnd + e1 + e2 + e3 + e4 + d1 + d2 + d3 + d4);
				Delta.emplace(sStart + s1 + s2 + s3 + s4, vec);
			}
		}
	}
	else
		std::cout << "The file could not be opened for reading\n";
}

bool M_TM::IsFinalState(const std::string& state) const
{
	for (auto State : f)
		if (State == state)
			return true;
	return false;
}

void M_TM::MoveRW_Head(int& ind, char d)
{
	switch(d) {
	case 'R':	++ind;
				break;
	case 'L':   --ind;
				break;
	case 'S':   break;
	}
}

void M_TM::PrintGame()
{
	for (int index = 0; index < 3; ++index)
		if (Kw[index + 1] == 'X' || Kw[index + 1] == 'O')
			Board[index] = Kw[index + 1];

	for (int index = 3; index < 6; ++index)
		if (Kw[index + 2] == 'X' || Kw[index + 2] == 'O')
			Board[index] = Kw[index + 2];

	for (int index = 6; index < 9; ++index)
		if (Kw[index + 3] == 'X' || Kw[index + 3] == 'O')
			Board[index] = Kw[index + 3];

	std::cout << char(201) << char(205) << char(203) << char(205) << char(203) << char(205) << char(187) << '\n';
	std::cout << char(186) << Board[0] << char(186) << Board[1] << char(186) << Board[2] << char(186) << '\n';
	std::cout << char(204) << char(205) << char(206) << char(205) << char(206) << char(205) << char(185) << '\n';
	std::cout << char(186) << Board[3] << char(186) << Board[4] << char(186) << Board[5] << char(186) << '\n';
	std::cout << char(204) << char(205) << char(206) << char(205) << char(206) << char(205) << char(185) << '\n';
	std::cout << char(186) << Board[6] << char(186) << Board[7] << char(186) << Board[8] << char(186) << '\n';
	std::cout << char(200) << char(205) << char(202) << char(205) << char(202) << char(205) << char(188) << '\n';
	std::cout << '\n';
}

void M_TM::StartGame(char player)
{
	Initialization(player);

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::mt19937 generator(seed);  

	int numOfX = 0;
	int numOfO = 0;
	int size;

	std::string aux;
	std::string State = q0;
	std::string State2;

	char s1, s2, s3, s4, e1, e2, e3, e4, d1, d2, d3, d4;
	bool crash = false;

	while (!IsFinalState(State) && !crash) {
		s1 = Ki[i];
		s2 = Kp[k1];
		s3 = Ko[k2];
		s4 = Kw[w];

		if (State == "qcp" && s4 != 'X')
			numOfX = 0;
		if (State == "qco" && s4 != 'O')
			numOfO = 0;

		if (Delta.find(State + s1 + s2 + s3 + s4) != Delta.end()) {
			aux = Delta.at(State + s1 + s2 + s3 + s4)[generator() % Delta.at(State + s1 + s2 + s3 + s4).size()];
	
			size = aux.size();
			d4 = aux[size - 1];
			d3 = aux[size - 2];
			d2 = aux[size - 3];
			d1 = aux[size - 4];
			e4 = aux[size - 5];
			e3 = aux[size - 6];
			e2 = aux[size - 7];
			e1 = aux[size - 8]; 
			State2.assign(aux, 0, aux.size() - 8);

			if ((State == "qp" || State == "qo") && State2 == "q1")
				PrintGame();
			Ki[i] = e1;
			Kp[k1] = e2;
			Ko[k2] = e3;
			Kw[w] = e4;
			
			MoveRW_Head(i, d1);
			MoveRW_Head(k1, d2);
			MoveRW_Head(k2, d3);
			MoveRW_Head(w, d4);

			State = State2;
		}
		else if (State == "qcp" && s4 == 'X') {
			numOfX++;
			if (numOfX == 3) {
				State = "Wp";
			}
			MoveRW_Head(w, 'L');
		}
		else if (State == "qco" && s4 == 'O') {
			numOfO++;
			if (numOfO == 3) {
				State = "Wo";
			}	
			MoveRW_Head(w, 'L');
		}
		else
		{
			std::cout << "Crash\n";
			crash = true;
		}
	}

	PrintGame();
	if (State == "Wp")
		std::cout << "Player X win\n";
	else
		if (State == "Wo")
			std::cout << "Player O win\n";
		else
			if (State == "Qd")
			 std::cout << "It's a draw\n";
}