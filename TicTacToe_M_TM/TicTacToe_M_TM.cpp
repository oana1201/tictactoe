#include <iostream>
#include "M_TM.h"

int main()
{
    M_TM turing;
    char option;

    turing.ReadFromFile("input.txt");
    std::cout << "Who starts the game? X/0\n";
    std::cin >> option;
    if (option == 'X')
        turing.StartGame('p');
    else
        turing.StartGame('o');
}

