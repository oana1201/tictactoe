#pragma once
#include <vector>
#include <string>
#include <unordered_map>

class M_TM
{
public:
	void ReadFromFile(const std::string& myFile);
	void StartGame(char player);

private:
	void Initialization(char player);
	bool IsFinalState(const std::string& state) const;
	void MoveRW_Head(int& ind, char d);
	void PrintGame();

private:
	std::vector<char> Ki;        // Input Tape
	std::vector<char> Kp;        // Player p's Tape
	std::vector<char> Ko;        // Player o's Tape
	std::vector<char> Kw;        // Winning Sequence Tape
	int i;                       // Index of Ki R/W head
	int k1;                      // Index of Kp R/W head
	int k2;                      // Index of Ko R/W head
	int w;                       // Index of Kw R/W head

	std::vector<std::string> Q;  
	std::string Gama;
	std::unordered_map<std::string, std::vector<std::string>> Delta;
	std::string q0;
	std::string b;
	std::string Sigma;
	std::vector<std::string> f;

	std::string Board;
};

